# README #

## timestamp_panel ##

### Description ###

Displays and formats the current date and time. 

Primarily for use to capture the date/time as a parameter in chains for use in subsequent steps, e.g. output file 20210605-My_Design.12daz

### Compilation ###

#### Source Files ####
There is a single source file included in this project:

1. **timestamp_panel.4dm** - the main source file. This is the one that should be compiled

And three additional files that you will be required to provide:

2. **set_ups.h** - shipped with 12d Model in the Program Files\Set_ups folder
3. **standard_library.h** - available for download from here- http://forums.12dmodel.com/downloads/Macros/includes.zip
4. **text_library.h** - available for download from here- http://forums.12dmodel.com/downloads/Macros/includes.zip

#### Instructions ####
1. Place all the files in the same directory.
2. Compile from within 12d Model via the Utilities->Macros->Compile menu by browsing for and selecting the main source file (*.4dm)
3. If you have compilation errors, they should be written to a compile_log.4dl file and displayed in your default text editor.
4. If compilation is successful, a compiled macro file, *.4do, will be created.

Pre-compiled versions for 12d Model V14 are also available in the **bin** folder.

### Running ###

The compiled macro (*.4do) can be run by drag'n'drop into an open 12d Model session, from the menu Utilities->Macros->Run->Run or via customised toolbars, menus, chains, screen layout files and much more.  Refer to the 12d Model help for details on customisation.

### Other ###

This is an application (aka macro) written specifically for 12d Model software using the 12d Programming Language (12dPL).

The application requires 12d Model to run and can only be run from within a 12d Model session.

The application was written targeting 12d Model V14C2h or newer, but should be able to compiled for V12 (at least) and perhaps earlier versions.
