/*****************************************
 *
 *	FILE:		timestamp_panel.4dm
 *
 *	DESCRIPTION:
 *	This macro shows the current date and time in a given
 *	format.  This is intended mainly for use in chains, where
 *	the current date/time may need to be captured as a parameter
 *	and re-used.
 *
 *	For example, this macro could be used to automatically export
 *	files with a timestamp.
 *
 *
 *
 *	AUTHOR:		Matthew Monk
 *	STARTED:	21/07/2014
 *
 *	REVISION HISTORY:
 *	1		21-07-2014	Initial macro
 *	2		06-02-2015	*ADD* Added command-line support
 *	3		09-02-2015	*FIX* Set format value on [Update] button for chains
 *						*ADD* Option to replace invalid filename characters
 *	4		04-06-2021	*MOD* Updated for V14 and source handover
 *
 */

#define DEBUG	0

// Prerequisites
// Standard 12d libraries
#include "set_ups.h"
#include "standard_library.h"
#include "text_library.h"

//
#define PRESET_DATE						1
#define PRESET_DATE_REVERSE				2
#define PRESET_TIME_SECONDS				3
#define PRESET_TIME_MINUTES				4
#define PRESET_ISO8601					5
#define PRESET_LONG_DATE				6
#define PRESET_LONG_DATE_TIME			7
#define PRESET_CUSTOM					8

#define PRESET_COUNT					8

#define DEFAULT_PRESET					PRESET_DATE_REVERSE
#define DEFAULT_REPLACE_INVALID_CHARS	FALSE
#define DEFAULT_FIELD_WIDTH_IN_CHARS	30

// #### GLOBAL VARIABLES ####
{
	// Appears in the panel title bar and about information printed to Output Window
	Text PROGRAM_NAME = "Show timestamp";
	Text PROGRAM_AUTHOR = "Matthew Monk";
	Text PROGRAM_VERSION = "1.3";
	Text PROGRAM_DATE = "2021-06-05";

}

// ### Function Prototypes ###

Integer Manage_panel(Integer cmd_line, Integer preset_mode, Text format_string);

Integer Get_preset_info(Integer mode, Text &label, Text &format);
void Populate_preset_labels(Text &labels[]);


Integer Validate_preset_modes(Choice_Box &modes_box, Integer num_modes, Text &modes[], Integer &mode);
Integer Set_custom_format_value(Integer mode, Integer cmd_line, Text &format, Input_Box &format_box);

Integer Show_timestamp(Integer time, Text format, Input_Box &display_box, Colour_Message_Box &msg);

Integer Display_current_timestamp(Input_Box &display_box, Text format, Integer replace_invalid, Colour_Message_Box &msg);
Integer Replace_invalid_filename_chars(Text input, Text &output);

/*! @ingroup macro
 *	@brief
 *	Main entry point for macro.
 */
void main()
{
	Integer rv = -1;

	#if DEBUG
		Clear_console();
	#endif

	Integer cmd_line = FALSE;
	Integer preset_mode = DEFAULT_PRESET;
	Text format_string = "";

	// Arguments
	Integer argc = Get_number_of_command_arguments();
	if (argc)
	{
		Text arg1 = "";
		rv = Get_command_argument(1, arg1);
		if (rv != 0)
		{
			Print("Error getting command line argument 1 to macro (err: " + To_text(rv) + ")\n");
			return;
		}
		cmd_line = TRUE;
		preset_mode = PRESET_CUSTOM;
		format_string = arg1;
	}
	rv = Manage_panel(cmd_line, preset_mode, format_string);
	if (rv != 0)
	{

	}
	return;
}

/*! @ingroup macro
 *	@brief
 *	Displays a GUI panel and handles user interaction with panel.
 */
Integer Manage_panel(Integer cmd_line, Integer preset_mode, Text format_string)
{
	Integer rv = -1;

	if ((preset_mode <= 0) || (preset_mode > PRESET_COUNT))
	{
		Print("Invalid prest mode (" + To_text(preset_mode) + "). " +
				"Must be between 1 and " + To_text(PRESET_COUNT) + ".\n");
		return 1;
	}

	#if VERSION_4D >= 1400
	Panel panel = Create_panel(PROGRAM_NAME, TRUE);
	#else
	Panel panel = Create_panel(PROGRAM_NAME);
	#endif // VERSION_4D

	// ### V & H GROUPS ###
	Vertical_Group vg_panel = Create_vertical_group(BALANCE_WIDGETS_OVER_HEIGHT);
	Vertical_Group vg_src = Create_vertical_group(ALL_WIDGETS_OWN_HEIGHT);
	Vertical_Group vg_main = Create_vertical_group(ALL_WIDGETS_OWN_HEIGHT);
	Vertical_Group vg_target = Create_vertical_group(ALL_WIDGETS_OWN_HEIGHT);

	Horizontal_Group hg_buttons = Create_button_group();

	Colour_Message_Box message = Create_colour_message_box(" ");

	// ### MAIN ###
	Integer num_presets = PRESET_COUNT;
	Text preset_modes[num_presets];
	Populate_preset_labels(preset_modes);

	Choice_Box preset_modes_box = Create_choice_box("Preset mode", message);
	Set_data(preset_modes_box, num_presets, preset_modes);
	Set_data(preset_modes_box, preset_modes[preset_mode]);

	Set_width_in_chars(preset_modes_box, DEFAULT_FIELD_WIDTH_IN_CHARS);
	Set_tooltip(preset_modes_box, "A list of common format presets. Select 'Custom' to enter your own.");

	Input_Box custom_format_box = Create_input_box("Format", message);
	Set_enable(custom_format_box, (preset_mode == PRESET_CUSTOM));
	if (cmd_line)
	{
		Set_data(custom_format_box, format_string);
	}
	Set_width_in_chars(custom_format_box, DEFAULT_FIELD_WIDTH_IN_CHARS);
	Set_tooltip(custom_format_box, "Format string for displaying the timestamp. Refer to help.");

	Named_Tick_Box replace_invalid_box = Create_named_tick_box("Replace invalid filename characters",
																DEFAULT_REPLACE_INVALID_CHARS, "replace_invalid");
	Set_tooltip(replace_invalid_box, "If ticked, invalid characters for filenames  \ / : * ? \" < > |  will be replaced with spaces");

	Append(preset_modes_box, vg_main);
	Append(custom_format_box, vg_main);
	Append(replace_invalid_box, vg_main);

	// ### TARGET BOX ###
	Input_Box timestamp_box = Create_input_box("Timestamp", message);
	Set_width_in_chars(timestamp_box, DEFAULT_FIELD_WIDTH_IN_CHARS);
	Set_tooltip(timestamp_box, "Resulting timestamp displayed here");

	Append(timestamp_box, vg_target);

	// ### BUTTONS ###
	Button but_process = Create_button("Update", "process");
	Button but_finish = Create_finish_button("Finish", "finish");
	Button but_help = Create_help_button(panel, "Help");

	Append(but_process, hg_buttons);
	Append(but_finish, hg_buttons);
	Append(but_help, hg_buttons);

	Append(vg_src, vg_panel);
	Append(vg_main, vg_panel);
	Append(vg_target, vg_panel);

	Append(message, vg_panel);
	Append(hg_buttons, vg_panel);

	Append(vg_panel, panel);

	Show_widget(panel);

	Integer doit = 1;
	Integer replace_invalid = DEFAULT_REPLACE_INVALID_CHARS;

	rv = Validate_preset_modes(preset_modes_box, num_presets, preset_modes, preset_mode);
	rv = Set_custom_format_value(preset_mode, cmd_line, format_string, custom_format_box);
	rv = Display_current_timestamp(timestamp_box, format_string, replace_invalid, message);

	while(doit)
	{
		Integer id = -1;
		Text    cmd = "";
		Text    msg = "";
		Integer ret = Wait_on_widgets(id,cmd,msg);  // this processes standard messages first ?

		rv = -1;

		switch(id)
		{
			case Get_id(panel) :
			{
				if(cmd == "Panel Quit")
				{
					doit = 0;
				}
				if(cmd == "Panel About")
				{
					// Standard 12d About dialog
					about_panel(panel);
				}
			} break;		// End - Panel

			case Get_id(but_finish) :
			{
				if(cmd == "finish") doit = 0;
			} break;		// End - finish

			case Get_id(preset_modes_box) :
			{
				if (cmd == "text selected")
				{
					preset_mode = 0;
					format_string = "";
					rv = Validate_preset_modes(preset_modes_box, num_presets, preset_modes, preset_mode);
					if (rv != 0)	break;

					rv = Set_custom_format_value(preset_mode, cmd_line, format_string, custom_format_box);

					rv = Display_current_timestamp(timestamp_box, format_string, replace_invalid, message);
				}
			} break;

			case Get_id(replace_invalid_box) :
			{
				rv = Validate(replace_invalid_box, replace_invalid);
			} break;

			case Get_id(but_process) :
			{
				// Main
				preset_mode = 0;
				format_string = "";

				Set_level(message, MESSAGE_LEVEL_GENERAL);

				rv = Validate_preset_modes(preset_modes_box, num_presets, preset_modes, preset_mode);
				if (rv != 0)	break;

				rv = Set_custom_format_value(preset_mode, cmd_line, format_string, custom_format_box);
				if (rv != 0)
				{

					break;
				}

				rv = Validate(custom_format_box, format_string);
				if (rv != TRUE)	break;

				rv = Validate(replace_invalid_box, replace_invalid);

				Set_data(timestamp_box, "");

				rv = Display_current_timestamp(timestamp_box, format_string, replace_invalid, message);
				if (rv == 0)
				{
					Set_data(message, "Finished.", MESSAGE_LEVEL_GOOD);
					Integer move_cursor = (Getenv("MOVE_CURSOR_TO_FINISH_BUTTON_4D") == "1") ? TRUE : FALSE;
					Set_finish_button(but_finish, move_cursor);
				}
				else
				{

				}
			} break;		// End - process
		}
	}
	return 0;
}

// Validate choice box of presets returning the index of the selection.
// returns 0 for success, non-zero for error/problem.
Integer Validate_preset_modes(Choice_Box &modes_box, Integer num_modes, Text &modes[], Integer &mode)
{
	Integer rv = -1;
	Text value = "";

	rv = Validate(modes_box, value);
	if ( (rv != TRUE) || (value == "") )
	{
		return 1;			// Invalid data
	}

	for (Integer i = 1; i <= num_modes; ++i)
	{
		if (modes[i] == value)
		{
			mode = i;
			return 0;
		}
	}
	return 2;
}

// Get the format string for the given mode if not custom
// and populate the format box with the value
Integer Set_custom_format_value(Integer mode, Integer cmd_line, Text &format, Input_Box &format_box)
{
	Integer rv = -1;

	if ((!cmd_line) && (mode != PRESET_CUSTOM))
	{
		Text label = "";
		rv = Get_preset_info(mode, label, format);
		if (rv != 0)
		{
			Print("Invalid or unknown prest (" + To_text(mode) + ")\n");
			return rv;
		}
	}
	rv = Set_data(format_box, format);
	if (rv != 0)
	{
		Print("Error setting format value in widget (err: " + To_text(rv) + ")\n");
		return 2;
	}
	Set_enable(format_box, (mode == PRESET_CUSTOM));

	return 0;
}

// Get the current time, format into timestamp, and display in box
Integer Display_current_timestamp(Input_Box &display_box, Text format, Integer replace_invalid, Colour_Message_Box &msg)
{
	Integer rv = -1;
	Integer time = 0;
	Text timestamp = "";

	rv = Time(time);
	if (rv != 0)
	{
		Set_data(msg, "Error getting current time.", MESSAGE_LEVEL_ERROR);
		return 1;
	}
	if (trim(format) == "")
	{
		Set_data(msg, "Invalid format string", MESSAGE_LEVEL_ERROR);
		return 2;
	}
	rv = Convert_time(time, format, timestamp);
	if (rv != 0)
	{
		Set_data(msg, "Error converting time. Check format string.", MESSAGE_LEVEL_ERROR);
		return 3;
	}
	if (replace_invalid)
	{
		//Print("Replacing invalid characters\n");
		rv = Replace_invalid_filename_chars(timestamp, timestamp);
		if (rv != 0)
		{
			Set_data(msg, "Error replacing invalid characters. Invalid characters may still be present.", MESSAGE_LEVEL_ERROR);
			return 5;
		}
	}
	//Print(timestamp + "\n");
	rv = Set_data(display_box, timestamp);
	if (rv != 0)
	{
		Set_data(msg, "Unable to set timestamp widget value");
		return 4;
	}
	Set_data(msg, "Timestamp updated.", MESSAGE_LEVEL_GOOD);
	return 0;
}

// Get the label and format string for a given preset
Integer Get_preset_info(Integer mode, Text &label, Text &format)
{
	label = format = "";
	switch (mode)
	{
		case (PRESET_DATE) :
		{
			label = "Date (DD-MM-YYYY)";
			format = "%d-%m-%Y";
		} break;

		case (PRESET_DATE_REVERSE) :
		{
			label = "Date reverse (YYYY-MM-DD)";
			format = "%Y-%m-%d";
		} break;

		case (PRESET_TIME_SECONDS) :
		{
			label = "Time (HH:MM:SS)";
			format = "%H:%M:%S";
		} break;

		case (PRESET_TIME_MINUTES) :
		{
			label = "Time to minutes (HH:MM)";
			format = "%H:%M";
		} break;

		case (PRESET_ISO8601) :
		{
			label = "Timestamp ISO 8601 (YYYY-MM-DDTHH:MM:SS)";
			format = "%Y-%m-%dT%H:%M:%S";
		} break;

		case (PRESET_LONG_DATE) :
		{
			label = "Long date (current locale)";
			format = "%#x";
		} break;

		case (PRESET_LONG_DATE_TIME) :
		{
			label = "Long date and time (current locale)";
			format = "%#c";
		} break;

		case (PRESET_CUSTOM) :
		{
			label = "Custom";
			format = "";
		} break;

		default :
		{
			return 1;
		} break;
	}
	return 0;
}

// Populate the array of prest labels
void Populate_preset_labels(Text &labels[])
{
	for ( Integer i = 1; i <= PRESET_COUNT; ++i )
	{
		Integer rv = -1;
		Text label, format;
		label = format = "";
		rv = Get_preset_info(i, label, format);
		if (rv == 0)
		{
			labels[i] = label;
		}
	}
	return;
}

Integer Replace_invalid_filename_chars(Text input, Text &output)
{
	Integer rv = -1;

	Integer num_errors = 0;
	Text invalid_chars = "\\/:*?\"<>|";
	Text replace_char = " ";

	Integer length = Text_length(input);
	output = input;

	for (Integer i = 1; i <= length; ++i )
	{
		Text curr_char = Get_subtext(output, i, i);
		if (Find_text(invalid_chars, curr_char))
		{
			Set_subtext(output, i, replace_char);
		}
	}
	return 0;
}
